import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import hello.Application;
//import hello.ParcelController;
import hello.Parcel;
import hello.ParcelController;
import hello.ParcelRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import java.nio.charset.Charset;
import static org.hamcrest.CoreMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {Application.class, ParcelController.class})

public class ParcelSearchTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    ParcelRepository parcelRep;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        //Delete records first
        parcelRep.deleteAll();
        Parcel parcel1 = new Parcel();
        parcel1.setParcelId("001");
        parcelRep.save(parcel1);

        Parcel parcel2 = new Parcel();
        parcel2.setParcelId("002");
        parcelRep.save(parcel2);

    }

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Test
    public void serviceAvailable() throws Exception {
        mockMvc.perform(get("/parcel"))
                .andExpect(status().isOk());
    }

    @Test
    public void readParcel1() throws Exception {
        mockMvc.perform(get("/parcel/search/001"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.parcelID", is("001")));
    }

    @Test
    public void readParcel2() throws Exception {
        mockMvc.perform(get("/parcel/search/002"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.parcelID", is("002")));
    }
}