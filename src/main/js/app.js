const React = require('react');
const ReactDOM = require('react-dom')
const client = require('./client');

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {parcels: []};
	}

	componentDidMount() {
		client({method: 'GET', path: '/parcel'}).done(response => {
			this.setState({parcels: response.entity._embedded.parcels});
		});
	}

	render() {
		return (
			<ParcelList parcels={this.state.parcels}/>
		)
	}
}

class ParcelList extends React.Component{
	render() {
		var parcels = this.props.parcels.map(parcel =>
			<Parcel key={parcel._links.self.href} parcel={parcel}/>
		);
		return (
			<table>
				<tbody>
					<tr>
						<th>Parcel ID</th>
						<th>First Name</th>
						<th>Second Name</th>
					</tr>
					{parcels}
				</tbody>
			</table>
		)
	}
}

class Parcel extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.parcel.parcelID}</td>
				<td>{this.props.parcel.customerFirstName}</td>
				<td>{this.props.parcel.customerSecondName}</td>
			</tr>
		)
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)