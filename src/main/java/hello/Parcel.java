package hello;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Will.Nash on 20/12/2016.
 */
@Document(collection = "parcel")
public class Parcel {

    @Id
    private String id;
    private String parcelID;
    private String orderNumber;
    private String delDepot;
    private String customerNumber;
    private String customerFirstName;
    private String customerLastName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getparcelID() {
        return parcelID;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getDelDepot() {
        return delDepot;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerName(String customerFirstName, String customerLastName) {
        this.customerFirstName = customerFirstName;
        this.customerLastName = customerLastName;
    }

    public void setParcelId(String parcelId) {
        parcelID = parcelId;
    }

}
