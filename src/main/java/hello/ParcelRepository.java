package hello;

/**
 * Created by Will.Nash on 20/12/2016.
 */
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

public interface ParcelRepository  extends MongoRepository<Parcel, String> {
    public Parcel findByParcelID(String parcelID);
}
