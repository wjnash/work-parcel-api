package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Will.Nash on 22/12/2016.
 */

@RestController
@RequestMapping("/parcel")
public class ParcelController {

    @Autowired
    ParcelRepository parcelRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Parcel> get(){
        return parcelRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Parcel create(@RequestBody Parcel parcel){

        Parcel result = parcelRepository.save(parcel);
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value="/search/{parcelID}")
    public Parcel get(@PathVariable String parcelID){


       if (parcelRepository.findByParcelID(parcelID) != null) {
            return parcelRepository.findByParcelID(parcelID);
        }


        return null;
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/{parcelID}")
    public void destroy(@PathVariable String parcelID) {
        parcelRepository.delete(parcelID);
    }

   /* @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound(String parcelID) {

    }*/
}
